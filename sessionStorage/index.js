function guardarEnLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value;
  var valor = txtValor.value;
  sessionStorage.setItem(clave, valor);
  var objeto = {
    nombre:"Ezequiel",
    apellidos:"Llarena Borges",
    ciudad:"Madrid",
    pais:"España"
  };
   sessionStorage.setItem("json", JSON.stringify(objeto));
}
function leerDeLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor;
  var datosUsuario = JSON.parse(sessionStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);
}
function removerEnLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.getItem(clave);
  sessionStorage.removeItem(txtClave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = "Eliminado el valor:  " + valor;
}
function borrarEnLocalStorage() {
  sessionStorage.clear();
}
function tamanoEnLocalStorage() {
  var tamano = sessionStorage.length;
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = "El tamano es:  " + tamano;
} 
